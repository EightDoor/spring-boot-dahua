package cn.start6.dahuacontrol.common;

public interface DeviceManagerListener {
	void onDeviceManager(String deviceId, String username, String password);
}
