package cn.start6.dahuacontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DahuaControlApplication {

    public static void main(String[] args) {
        SpringApplication.run(DahuaControlApplication.class, args);
    }
}
